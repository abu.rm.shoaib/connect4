from connect4 import Grid, GameParameters, Game
import pytest


class TestGrid:
    @pytest.fixture
    def grid(self):
        return Grid()

    @pytest.mark.parametrize("column,expected", [
        (0, "......"),
        (6, "......")
    ])
    def test_get_column_with_no_tokens(self, grid, column, expected):
        assert grid.get_column(column) == expected

    @pytest.mark.parametrize("row,expected", [
        (0, "......."),
        (5, ".......")
    ])
    def test_get_row_with_no_tokens(self, grid, row, expected):
        assert grid.get_row(row) == expected

    def test_get_out_of_bounds_column_raises_index_error(self, grid):
        with pytest.raises(IndexError):
            grid.get_column(7)

    def test_get_out_of_bounds_row_raises_index_error(self, grid):
        with pytest.raises(IndexError):
            grid.get_row(7)

    @pytest.mark.parametrize("column,row,expected", [
        (0, 0, "......"),
        (0, 4, "..")
    ])
    def test_get_positive_diagonal_with_no_tokens(self, grid, column, row, expected):
        assert grid.get_diagonal(column, row, GameParameters.GRADIENT_UP) == expected

    @pytest.mark.parametrize("column,row,expected", [
        (0, 5, "......"),
        (3, 5, "....")
    ])
    def test_get_negative_diagonal_with_no_tokens(self, grid, column, row, expected):
        assert grid.get_diagonal(column, row, GameParameters.GRADIENT_DOWN) == expected

    @pytest.mark.parametrize("moves,expected_column,expected_row", [
        ([("x", 0)], 0, 0),
        ([("x", 0), ("o", 0)], 0, 1),
        ([("x", 2), ("o", 2), ("x", 2)], 2, 2)
    ])
    def test_add_token_and_check_return_values(self, grid, moves, expected_column, expected_row):
        for token, column in moves:
            column, row = grid.add_token_to_grid(token, column)
            
        assert column == expected_column
        assert row == expected_row
    
    @pytest.mark.parametrize("moves,column_to_check,expected", [
        ([("x", 0)], 0, "x....."),
        ([("x", 0), ("o", 0)], 0, "xo...."),
        ([("x", 4)], 0, "......"),
        ([("x", 4)], 4, "x.....")
    ])
    def test_add_token_and_check_column(self, grid, moves, column_to_check, expected):
        for token, column in moves:
            grid.add_token_to_grid(token, column)

        assert grid.get_column(column_to_check) == expected

    @pytest.mark.parametrize("moves,row_to_check,expected", [
        ([("x", 0)], 0, "x......"),
        ([("x", 0), ("o", 0)], 1, "o......"),
        ([("x", 4), ("x", 1), ("x", 5)], 0, ".x..xx.")
    ])
    def test_add_token_and_check_row(self, grid, moves, row_to_check, expected):
        for token, column in moves:
            grid.add_token_to_grid(token, column)

        assert grid.get_row(row_to_check) == expected

    @pytest.mark.parametrize("moves,diagonal_to_check,expected", [
        ([("x", 0)], (0, 0), "x....."),
        ([("x", 3), ("o", 3)], (3, 0), "x...")
    ])
    def test_add_token_and_check_positive_diagonal(self, grid, moves, diagonal_to_check, expected):
        for token, column in moves:
            grid.add_token_to_grid(token, column)

        assert grid.get_diagonal(diagonal_to_check[0], diagonal_to_check[1], GameParameters.GRADIENT_UP) == expected

    def test_grid_is_won_column(self, grid):
        grid.columns[0] = "xxxx.."

        assert grid.is_won(0, 3, "x") is True

    def test_grid_is_won_row(self, grid):
        grid.rows[4] = list(".oooo..")

        assert grid.is_won(0, 4, "o") is True

    def test_grid_is_won_diagonal(self, grid):
        grid.columns[1] = "xoxo.."
        grid.columns[2] = "xoox.."
        grid.columns[3] = "ooxx.."
        grid.columns[4] = "oxxx.."

        assert grid.is_won(1, 3, "o") is True

    def test_grid_is_full(self, grid):
        grid.columns[0] = "ooxoxo"
        grid.columns[1] = "xxooxx"
        grid.columns[2] = "xoxoxo"
        grid.columns[3] = "oxoxox"
        grid.columns[4] = "xoxxoo"
        grid.columns[5] = "oxxoox"
        grid.columns[6] = "oooxxx"
        
        assert grid.is_full() is True


class TestGame:
    @pytest.fixture
    def game(self):
        return Game(Grid())

    def test_get_token_in_play(self, game):
        assert game.get_token_in_play() == 'o'

    def test_switch_player(self, game):
        assert game.get_token_in_play() == 'o'
        game.switch_player()
        assert game.get_token_in_play() == 'x'

    @pytest.mark.parametrize("move,expected", [
        ("0", True),
        ("4", True),
        ("-1", False),
        ("", False),
        ("foo", False),
        ("7", False)
    ])
    def test_move_is_valid(self, game, move, expected):
        assert game.move_is_valid(move) is expected

    def test_move_is_valid_when_column_full(self, game):
        game.grid.columns[0] = "xxxxxx"
        assert game.move_is_valid("0") is False
