# Connect4

The Connect4 game consists of a 7x6 grid, with `.` representing empty slots, `o` and `x` representing the two "colours" that can be played.

The game begins with the following prompt:

```
*** Welcome to Connect4! ***

Player 1 plays o
Player 2 plays x

The grid is ready.

0 1 2 3 4 5 6
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .

Player 1, playing o. Choose a column [0-6] to drop your token:
```

Choosing a column causes the token to be dropped into that column, e.g. choosing 0 gives:

```
0 1 2 3 4 5 6
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
o . . . . . .
```

The prompt then switches to the other player, and the game continues until someone wins or the grid is full (i.e. a draw).

## Explanation

The premise of the solution is based on checking substrings within a string. Columns, rows and diagonals are treated as a string of tokens played, and the presence of a winning sequence (`xxxx` or `oooo`) is checked.

This is trivial in Python using the `if substring in string` syntax and offloads computation to the Python interpreter.

For example, in the following grid:

```
5 . . . . . . .
4 . . . . . . .
3 . . o o . . .
2 . x o x . . .
1 x o x x . . .
0 o o x o . . .
  0 1 2 3 4 5 6
```

* Column 2 is represented as `xxoo..`
* Row 1 is represented as `xoxx...`
* Diagonal from (0,0) to (3,3) is represented as `oooo..`

In this example, the substring `oooo` exists within the diagonal `oooo..`, therefore the player playing `o` wins the game.

## Code structure

There are 3 classes comprising the solution.

### `GameParameters`
This just contains a few constants to set the length of tokens required to win the game, and the gradient of the diagonals.

### `Grid`
This defines the grid used in the game. The columns, rows and diagonals of the grid are represented as lists of strings:
* Columns are treated as stacks, and each new token is added to the stack. When fetching the column, extra `.`'s are padded to the end to represent unused slots.
* Rows are nested lists of strings rather than just strings to simplify insertion of tokens at a given column. Rows are stored alongside columns to reduce iterations when fetching rows to check a winning sequence.
* Diagonals are not pre-computed and instead dynamically generated when requested. This could probably be changed but requires additional data structures, which was skipped for the purposes of completing the exercise.

### `Game`
This contains the main game loop and keeps track of which colour token is currently in play.

## Running the game

The game requires Python 3.10+ to run. To run the game, execute:

```
$ python connect4.py
```

## Tests

Tests require `pytest`. If not installed, execute:

```
$ pip install pytest
```

To run tests:

```
$ pytest

==== test session starts ====
platform darwin -- Python 3.10.9, pytest-7.2.1, pluggy-1.0.0
rootdir: connect4
collected 35 items               

test_connect4.py ................................... [100%]

==== 35 passed in 0.02s ====
```