from itertools import cycle
from collections.abc import Iterator


class GameParameters:
    """
    Constants used for the game:
    * WINNING_LENGTH = Number of consecutive tokens (up, down or diagonally) needed to win
    * GRADIENT_UP / DOWN = Gradients used to calculate diagonal points
    """
    WINNING_LENGTH: int = 4
    GRADIENT_UP: int = 1
    GRADIENT_DOWN: int = -1


class Grid:
    """
    Constants used for the grid:
    * WIDTH / HEIGHT = Width and height of the game grid
    """
    WIDTH: int = 7
    HEIGHT: int = 6

    def __init__(self) -> None:
        """
        We initialise the grid columns and rows here
        to be used in the rest of the game. The README
        provides some reasoning behind their structure.
        """
        self.columns: list[str] = [''] * Grid.WIDTH
        self.rows: list[list[str]] = [list('.' * Grid.WIDTH) for i in range(Grid.HEIGHT)]

    def get_column(self, column: int) -> str:
        """
        Returns the column requested, padded with . to present
        the full column as a string including empty slots.
        """
        return self.columns[column].ljust(Grid.HEIGHT, '.')

    def get_row(self, row: int) -> str:
        """
        Returns the entire row requested as a string.
        """
        return ''.join(self.rows[row])

    def get_diagonal(self, column: int, row: int, gradient: int) -> str:
        """
        Returns the diagonal requested as a string,
        which is extrapolated from the given row and column co-ordinates
        at the provided gradient (i.e. positive or negative).
        """
        diagonal: str = ''

        # Treat the diagonal as a gradient on a graph translated by some amount
        # for a given row and column co-ordinate
        translation: int = row + (column * -gradient)

        # Knowing the translation, we can use an equation of the form "y = t + gx"
        # where t is the translation and g is the gradient, to get all grid points
        # and therefore tokens that lie on the diagonal.
        for x in range(Grid.WIDTH):
            y: int = translation + (x * gradient)
            if y < Grid.HEIGHT and y >= 0:
                diagonal += self.get_column(x)[y]
        
        return diagonal

    def is_won(self, column: int, row: int, token: str) -> bool:
        """
        Checks if the token provided at the column and row
        completes a winning sequence.
        """
        sequence: str = token * GameParameters.WINNING_LENGTH
        
        return sequence in self.get_column(column) or \
            sequence in self.get_row(row) or \
            sequence in self.get_diagonal(column, row, GameParameters.GRADIENT_UP) or \
            sequence in self.get_diagonal(column, row, GameParameters.GRADIENT_DOWN)

    def is_full(self) -> bool:
        """
        Checks if the grid is full and there are no slots for
        new tokens to be added.
        """
        for c in range(Grid.WIDTH):
            if '.' in self.get_column(c):
                return False

        return True

    def add_token_to_grid(self, token: str, column: int) -> tuple[int, int]:
        """
        Adds the provided token to the provided column,
        and returns the column and row where the token was added.
        """
        self.columns[column] += token

        # Also update the row with the column position and token.
        # Since the column is a stack, the row is just the length of the stack.
        row: int = len(self.columns[column]) - 1
        self.rows[row][column] = token

        return column, row

    def render_grid(self) -> str:
        """
        Renders out the current grid, e.g.:
                
        0 1 2 3 4 5 6
        . . . . . . .
        . . . . . . .
        x . . . . . .
        o x . . . . .
        o o x . . . .
        o o x x . . .
        """
        # Could generate this instead based on Grid.WIDTH
        grid_string: str = "\n0 1 2 3 4 5 6\n"
        
        for r in self.rows[::-1]:
            grid_string += ' '.join(r) + "\n"

        return grid_string


class Game:
    """
    Constants used for the game:
    * TOKENS = The two colours used for the game
    """
    TOKENS: tuple[str, str] = 'o', 'x'

    def __init__(self, grid: Grid) -> None:
        self.players: Iterator[int] = cycle(range(2))
        self.current_player: int = next(self.players)
        self.grid: Grid = grid

    def switch_player(self) -> None:
        self.current_player = next(self.players)

    def get_token_in_play(self) -> str:
        return Game.TOKENS[self.current_player]

    def get_player_move(self) -> int:
        """
        Prompts for player input for the next move.
        If the move is not valid, recursively calls itself
        until a valid move is provided (or call stack limit is reached!)
        """
        prompt_text: str = "Player {}, playing {}. Choose a column [0-{}] to drop your token: ".format(
            self.current_player + 1, # + 1 because self.players is 0-indexed
            self.get_token_in_play(),
            Grid.WIDTH - 1
        )

        move: str = input(prompt_text)
        if self.move_is_valid(move):
            return int(move)
        else:
            print("Move is not valid. Please try again.")
            return self.get_player_move()

    def move_is_valid(self, move: str) -> bool:
        """
        Move is valid if the column chosen is an integer,
        and the given column is within the grid size,
        and the column is not full.
        """
        if move.isdigit():
            column: int = int(move)
            if column >= 0 and column < Grid.WIDTH:
                return '.' in self.grid.get_column(column)

        return False

    def play(self) -> None:
        """
        The main game loop.

        After some initial text, the game goes into a loop until
        either someone wins or the grid is full.
        """
        
        print("\n*** Welcome to Connect4! ***\n")

        print("Player 1 plays {}".format(Game.TOKENS[0]))
        print("Player 2 plays {}".format(Game.TOKENS[1]))
        
        print("\nThe grid is ready.")
        print(self.grid.render_grid())

        while not self.grid.is_full():
            column, row = self.grid.add_token_to_grid(
                self.get_token_in_play(),
                self.get_player_move()
            )

            print(self.grid.render_grid())

            if self.grid.is_won(column, row, self.get_token_in_play()):
                print("*** GAME OVER ***")
                print("*** Player {} ({}) wins!!! ***".format(
                    self.current_player + 1,
                    self.get_token_in_play()
                ))
                return

            self.switch_player()
        
        print("*** GAME OVER ***")
        print("Game is a draw!")


if __name__ == "__main__":
    Game(Grid()).play()
